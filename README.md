# deGrid

A modern grid system that is not limited to one fixed number of columns, and relies on useful breakpoints.

## Objectives

* Useful breakpoints;
* Not limited to 12 columns (or other fixed number);
* Sensible values for nested columns;
* Based on powers of 2;
* Harmonic series for extra fanciness;
* Keep file size reasonable.

## Usage

<pre>
col-&lt;<var>breakpoint</var>&gt;-&lt;<var>numerator</var>&gt;o&lt;<var>denominator</var>&gt;
</pre>

### Example

<pre>
&lt;div class="col-xs-1o2"&gt;&lt;/div&gt;
</pre>

## Breakpoints

<pre>
    $breakpoints = [
        'xs' => 0,
        'sm' => 300,
        'md' => 600,
        'lg' => 900,
        'xl' => 1200,
        'xxl' => 1500,
        'xxxl' => 1800
    ];
</pre>

## Containers

<pre>
$containers = [
    'sm' => 264,
    'md' => 576,
    'lg' => 864,
    'xl' => 1176
];
</pre>

## Comparison

|            | deGrid | Bootstrap 4| Foundation 6 | 960 Grid System | Pure |
|------------|--------|-----------|------------|-----------------|---------|
| breakpoints | 7      | 5         | 3          | 1               | 5 |
| 10 columns | ✓      |           |            |                 | |
| 12 columns | ✓      | ✓         | ✓          | ✓               | ✓ |
| 16 columns | ✓      |           |            | ✓               | |
| 24 columns | ✓      |           |            |                | ✓ |
| harmonic columns | ✓      |           |            |                | |