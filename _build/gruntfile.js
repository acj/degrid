module.exports = function(grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        sass: {
            dist: {
                options: {
                    noCache: true,
                    sourceMap: false,
                    outputStyle: 'compressed'
                },
                files: {
                    '../dist/css/main.min.css': '../scss/degrid.scss'
                }
            }
        },
        watch: {
            css: {
                files: ['../scss/**/*.scss'],
                tasks: ["sass"]
            }
        }
    });

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.registerTask('build', ['sass']);
    grunt.registerTask('default', ['sass']);
};
