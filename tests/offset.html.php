<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>deGrid</title>
    <link href="https://alexanderchristiaanjacob.com/" rel="author">
    <link href="base.css" rel="stylesheet">
    <link href="../main.min.css" rel="stylesheet">
</head>
<body id="top">
<header>
    <h1>de<b>Grid</b></h1>
</header>
<main id="content" role="main">

    <div class="container">
        <div class="row">
            <div class="col-xs-1o1"><a href="./">↩ all tests</a></div>
        </div>
    </div>

    <section class="container">
        <h2>Offsets</h2>
        <p>Most column types have an offset counterpart. Space on the right side of the columns does not require special treatment.</p>
        <?php

        for($i = 2; $i <= 24; ++$i):
            echo '<div class="row">';

            for($j = $i; $j > 1; $j -= 2):
                echo '<div class="col-xs-1o' . $i . ' offset-xs-1o' . $i . '">1o' . $i . '</div>';
            endfor;

            echo '</div>';
        endfor;

        ?>
    </section>

</main>
<footer>©</footer>
</body>
</html>