<!DOCTYPE html>
<html dir="ltr" lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>deGrid</title>
    <link href="https://alexanderchristiaanjacob.com/" rel="author">
    <link href="base.css" rel="stylesheet">
    <link href="../main.min.css" rel="stylesheet">
</head>
<body id="top">
<header>
    <h1>de<b>Grid</b></h1>
</header>
<main id="content" role="main">

    <div class="container">
        <div class="row">
            <div class="col-xs-1o1"><a href="./">↩ all tests</a></div>
        </div>
    </div>

    <?php

    $breakpoints = [
        'xs' => 0,
        'sm' => 300,
        'md' => 600,
        'lg' => 900,
        'xl' => 1200,
        'xxl' => 1500,
        'xxxl' => 1800
    ];

    //$breakpoints = array_reverse($breakpoints);

    foreach($breakpoints as $b => $s):
        $r = [];

        echo '<section class="container" id="' . $b . '">';
        echo '<h2>' . strtoupper($b) . ' (' . $s . ' pixels)</h2>';

        for($d = 24; $d > 0; --$d):

            echo '<div class="row">';

            for($n = 1; $n <= $d; ++$n):

                echo '<div class="col-' . $b . '-1o' . $d . '">' . $n . ' of ' . $d . '</div>';

            endfor;

            echo '</div>';

        endfor;

        echo '</section>';

    endforeach;

    ?>
</main>
<footer>©</footer>
</body>
</html>